import Vue from "vue";
import App from "./App.vue";

import SuiVue from "semantic-ui-vue";
import "semantic-ui-css/semantic.css";
import responsive from "vue-responsive";
import Scrollspy from "vue2-scrollspy";
// import { Install as VBreakpoint } from 'vue-breakpoint-component'
import VueAnime from "vue-animejs";
import VueCarousel from "vue-carousel";
// eslint-disable-next-line
import store from "./store";
import VueResizeText from "vue-resize-text";
Vue.config.productionTip = false;
Vue.use(SuiVue);
new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
Vue.use(responsive);
Vue.use(Scrollspy);
// Vue.use(VBreakpoint)
Vue.use(VueAnime);

Vue.use(VueCarousel);

Vue.use(VueResizeText);
