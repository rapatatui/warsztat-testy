import * as axios from "axios";

const BASE_URL = "http://localhost";

function upload(formData) {
  const url = `${BASE_URL}`;
  return (
    axios
      .post(url, formData)
      // get data
      .then(x => x.data)
      // add url field
      .then(x =>
        x.map(img =>
          Object.assign({}, img, {
            url: +`${BASE_URL}/assets/${img.id}`
          })
        )
      )
  );
}

export { upload };
